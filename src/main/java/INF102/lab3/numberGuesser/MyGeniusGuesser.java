package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int higher = number.getUpperbound();
        int lower = number.getLowerbound();
        int guesser = (higher + lower) / 2;
        int result = number.guess(guesser);

        while ( result != 0) {
            if (result == -1) {
                lower = guesser;
            }
            else {
                higher = guesser;
            }
            guesser = (higher + lower) / 2;
            result = number.guess(guesser);
        }
        return guesser;
    }

}
