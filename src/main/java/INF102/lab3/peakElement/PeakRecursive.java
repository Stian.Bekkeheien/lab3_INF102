package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int s = numbers.size();
        return numbers.get(find(numbers, 0, s-1));
    }

    public int find(List<Integer> numbers, int l, int r) {
        
        int mid = (r + l) / 2;
        if (r==l){
            return l;
        }
        
        if (numbers.get(mid) > numbers.get(mid + 1)){
            return find(numbers, l, mid);
        }
        return find(numbers, mid + 1, r);

    }

}
