package INF102.lab3.sumList;

import java.util.List;

import javax.swing.text.DefaultEditorKit.CopyAction;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        int S = list.size();
        
        if (S == 0){
            return 0;}

        List<Long> sub = list.subList(1, S);
        
        return sum(sub) + list.get(0);
        
    }

}
